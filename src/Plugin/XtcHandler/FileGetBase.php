<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler for File.
 *
 */
abstract class FileGetBase extends FileBase
{

  public function runProcess(){
    if(file_exists($this->options['path'])){
      $this->content = file_get_contents($this->options['path']);
    }
  }

}
