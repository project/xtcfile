<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


use Drupal\Component\Serialization\Json;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "json_get",
 *   label = @Translation("JSON File for XTC"),
 *   description = @Translation("JSON File for XTC description.")
 * )
 */
class JsonGet extends FileGetBase
{

//  protected function adaptContent(){
//    $this->content = Json::decode($this->content);
//  }

}
