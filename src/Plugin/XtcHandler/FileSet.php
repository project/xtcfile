<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "file_set",
 *   label = @Translation("Create File or Update File content for XTC"),
 *   description = @Translation("Create File or Update File content for XTC description.")
 * )
 */
class FileSet extends FileCreate
{

  public function runProcess(){
    $this->write();
  }

}
