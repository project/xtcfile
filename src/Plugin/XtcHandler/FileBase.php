<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


use Drupal\Core\Site\Settings;
use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

/**
 * Plugin implementation of the xtc_handler for File.
 *
 */
abstract class FileBase extends XtcHandlerPluginBase {

  protected function initProcess() {
    $this->buildPath();
  }

  public function setOptions($options = []): XtcHandlerPluginBase {
    $options['path'] = $this->buildPath();
    $this->options = $options;
    return $this;
  }

  protected function buildPath() {
    if (!empty($this->profile['abs_path'])) {
      return $this->getFullPath();
    }

    if (!empty($this->profile['path_root'])) {
      switch ($this->profile['path_root']) {
        case 'config-sync':
          $pwd = Settings::get('config_sync_directory');
          break;
        case 'public':
          $pwd = $this->getFilesPath();
          break;
        case 'flysystem':
          $flySystems = Settings::get('flysystem');
          $flySystem = $flySystems[$this->profile['flysystem']];
          if ('local' == $flySystem['driver']) {
            $pwd = $flySystem['config']['root'];
          }
          break;
        default:
          $module_handler = \Drupal::service('module_handler');
          // TODO: Provide a container to test class to make it not fail here.
          $pwd = $module_handler->getModule($this->profile['module'])
            ->getPath();
      }
    }

    if (!empty($pwd)) {
      return $pwd . '/' . $this->getFullPath();
    }
    return $this->getFullPath();
  }

  protected function getFilesPath() {
    $file_default_scheme = \Drupal::config('system.file')
      ->get('default_scheme');
    return \Drupal::service('file_system')
      ->realpath($file_default_scheme . "://");
  }

  protected function getFullPath() {
    return $this->profile['path'];
  }

  public static function type() {
    return 'file';
  }

  /**
   * @return string[]
   */
  public static function getStruct(): array {
    return ['abs_path', 'path_root', 'module', 'path', 'filetype'];
  }

}
