<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


use Drupal\xtcfile\XtendedContent\API\LoadCsv;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "csv_get",
 *   label = @Translation("CSV File for XTC"),
 *   description = @Translation("CSV File for XTC description.")
 * )
 */
class CsvGet extends FileGetBase
{

  protected function adaptContent(){
    $delimiter = ';';
    $enclosure = '"';
    $heading = TRUE;
    $limit = NULL;
    $csv = New LoadCsv();
    $this->content = $csv->getContent($this->content, $delimiter, $enclosure, $heading, $limit);
  }


}
