<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "file_update",
 *   label = @Translation("Update File content for XTC"),
 *   description = @Translation("Update File content for XTC description.")
 * )
 */
class FileUpdate extends FileCreate
{

  public function runProcess(){
    if(file_exists($this->options['path'])){
      $this->write();
    }
  }

}
