<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "dir_append",
 *   label = @Translation("Append File content in Directory for XTC"),
 *   description = @Translation("Append File content in Directory for XTC description.")
 * )
 */
class DirAppend extends DirSet {

  protected function write() {
    $path = $this->options['path'];
    $dir = dirname($path);
    if (!is_dir($dir)) {
      \Drupal::service('file_system')->mkdir($dir, 0775, TRUE);
    }
    if (!empty($this->content)) {
      $this->filesize = file_put_contents($path, $this->content, FILE_APPEND | LOCK_EX);
    }
  }

}
