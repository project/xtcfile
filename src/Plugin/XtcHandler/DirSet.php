<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "dir_set",
 *   label = @Translation("Create File in Directory or Update File content for XTC"),
 *   description = @Translation("Create File in Directory or Update File in Directory content for XTC description.")
 * )
 */
class DirSet extends DirCreate {

  public function runProcess() {
    $this->write();
  }

}
