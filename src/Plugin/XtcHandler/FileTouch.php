<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "file_touch",
 *   label = @Translation("Touch File for XTC"),
 *   description = @Translation("Touch File for XTC description.")
 * )
 */
class FileTouch extends DirCreate {

  protected function write() {
    $path = $this->options['path'];
    $dir = dirname($path);
    if (!is_dir($dir)) {
      \Drupal::service('file_system')->mkdir($dir, 0775, TRUE);
    }
    $this->filesize = file_put_contents($path, $this->content);
  }

}
