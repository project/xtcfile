<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "file_cache",
 *   label = @Translation("Create or Update File Cache content for XTC"),
 *   description = @Translation("Create or Update File Cache content for XTC description.")
 * )
 */
class FileCache extends FileBase {

  protected $filesize;

  public function process() {
    $this->initProcess();
    $this->adaptContent();
    $this->runProcess();
    return $this;
  }

  public function runProcess() {
    if (file_exists($this->options['path'])) {
      return;
    }
    $this->write();
  }

  protected function write() {
    $path = $this->options['path'];
    $dir = dirname($path);
    if (!is_dir($dir)) {
      \Drupal::service('file_system')->mkdir($dir, 0775, TRUE);
    }
    $this->filesize = file_put_contents($path, $this->content);
  }

  /**
   * @return mixed
   */
  public function size() {
    return $this->filesize;
  }
}
