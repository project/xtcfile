<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "dir_create",
 *   label = @Translation("Create File in Directory for XTC"),
 *   description = @Translation("Create File in Directory for XTC description.")
 * )
 */
class DirCreate extends FileCreate {

  protected $filename;

  public function setOptions($options = []): XtcHandlerPluginBase {
    $this->filename = $options['filename'];
    $options['path'] = $this->buildPath();
    $this->options = $options;
    return $this;
  }

  public function setName($name) {
    $this->filename = $name;
    return $this;
  }

  protected  function getFullPath(){
    return $this->profile['path'] . '/' . $this->filename;
  }

}
