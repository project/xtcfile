<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


use Drupal\Core\Serialization\Yaml;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "yaml_get",
 *   label = @Translation("YAML File for XTC"),
 *   description = @Translation("YAML File for XTC description.")
 * )
 */
class YamlGet extends FileGetBase
{

  protected function adaptContent(){
    $this->content = Yaml::decode($this->content);
  }

}
