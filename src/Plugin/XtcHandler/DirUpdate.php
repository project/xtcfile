<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "dir_update",
 *   label = @Translation("Update File in Directory content for XTC"),
 *   description = @Translation("Update File in Directory content for XTC
 *   description.")
 * )
 */
class DirUpdate extends DirCreate {

  public function runProcess() {
    if (file_exists($this->options['path'])) {
      $this->write();
    }
  }

}
