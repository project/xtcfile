<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "file_create",
 *   label = @Translation("Create File for XTC"),
 *   description = @Translation("Create File for XTC description.")
 * )
 */
class FileCreate extends FileBase {

  protected $filesize;

  public function process() {
    $this->initProcess();
    $this->adaptContent();
    $this->runProcess();
    return $this;
  }

  public function runProcess() {
    if (file_exists($this->options['path'])) {
      return;
    }
    $this->write();
  }

  protected function write() {
    $path = $this->options['path'];
    $dir = dirname($path);
    if (!is_dir($dir)) {
      \Drupal::service('file_system')->mkdir($dir, 0775, TRUE);
    }
    if (!empty($this->content)) {
      $this->filesize = file_put_contents($path, $this->content);
    }
  }

  /**
   * @return mixed
   */
  public function size() {
    return $this->filesize;
  }
}
