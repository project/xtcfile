<?php

namespace Drupal\xtcfile\Plugin\XtcHandler;


use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "file_delete",
 *   label = @Translation("Delete File for XTC"),
 *   description = @Translation("Delete File for XTC description.")
 * )
 */
class FileDelete extends FileBase
{

  public function runProcess(){
    if(file_exists($this->options['path'])){
      unlink($this->options['path']);
    }
  }

}
