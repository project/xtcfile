<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-21
 * Time: 09:39
 */

namespace Drupal\xtcfile\XtendedContent\API;


use Drupal\Component\Serialization\Json;

class LoadJson
{

  public function getContent($content){
    return Json::decode($content);
  }

}
