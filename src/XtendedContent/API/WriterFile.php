<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcfile\XtendedContent\API;


use Drupal\xtc\Plugin\XtcHandler\TouchGet;
use Drupal\xtc\XtendedContent\API\WriterBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcfile\Plugin\XtcHandler\FileCreate;
use Drupal\xtcflysystem\Plugin\XtcHandler\FlySystemCreate;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class XtcWriterFile
 *
 * @package Drupal\xtcfile\XtendedContent\API
 */
class WriterFile extends WriterBase
{

  /**
   * @param $content
   * @param $name
   * @param array $options
   *
   * @return mixed|\Symfony\Component\HttpFoundation\Response
   */
  public static function write($content, $name, $options = []){
    if (!empty($options['handler'])) {
      $handler = XtcLoaderHandler::get($options['handler']);
    } else {
      $profile = XtcLoaderProfile::load($name);
      $handler_name = $profile['type'] . '_' . $profile['verb'];
      $handler = XtcLoaderHandler::get($handler_name);
    }
    $fullOptions = array_merge($profile, $options);
    $handler->setProfile($fullOptions);
    $handler->setOptions($fullOptions);
    if (!empty($handler) && (($handler instanceof FileCreate) || ($handler instanceof FlySystemCreate)) ) {
      $handler->writeContent($content);
      $path = $handler->getOptions()['path'] ?? '';
      if ($handler->size() > 0) {
        return New Response('file created / modified: ' . $path . '(' . $handler->size() . ')');
      }
      if(file_exists($path)) {
        return New Response('file exists: ' . $path);
      }
      return New Response('file not created / modified: ' . $path);
    }
  }
}
