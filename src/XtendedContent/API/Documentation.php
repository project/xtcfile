<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-31
 * Time: 11:59
 */

namespace Drupal\xtcfile\XtendedContent\API;


use Drupal\Core\Link;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtcfile\Controller\XtcDocumentationController;

/**
 * Class Documentation
 *
 * @package Drupal\xtcfile\XtendedContent\API
 */
class Documentation
{

  /**
   * @param $name
   *
   * @return string
   */
  public static function getHelp($module){
    $links =(New XtcDocumentationController())
      ->setModule($module)
      ->getHelp();
    return self::getHelpFile($module)
           . $links;
  }

  /**
   * @param $name
   *
   * @return string
   */
  public static function getHelpFile($module){
    foreach(['help/help.md'] as $path){
      $profile = [
        'type' => 'markdown',
        'verb' => 'get',
        'abs_path' => false,
        'module' => $module,
        'path' => $path,
      ];
      $content = self::getFromProfile($profile);
      if(!empty($content)){
        return $content;
      }
    }
    return '';
  }

  public static function getDocs($module){
    $profile = [
      'type' => 'mkdocs',
      'verb' => 'get',
      'abs_path' => false,
      'module' => $module,
      'path' => 'help/mkdocs.yml',
    ];
    $content = self::getFromProfile($profile);
    if(!empty($content) && is_array($content)) {
      return $content;
    }
    return "<h2>Documentation needs to be created.</h2>
           <p>Documentation follows <b><a href='https://www.mkdocs.org/' target='_blank'>
           mkdocs</a></b> standards.</p>
        ";
  }

  /**
   * @param $name
   *
   * @return string
   */
  public static function getDocsPage($module, $path){
    $profile = [
      'type' => 'mkdocs',
      'verb' => 'get',
      'abs_path' => false,
      'module' => $module,
      'path' => 'help/docs/' . $path,
    ];
    $content = self::getFromProfile($profile);
    if(!empty($content)) {
      return $content;
    }
    $link = Link::createFromRoute('Index', 'xtcfile.docs.docs',
                                  ['module' => $module])->toString();
    return "<h2>Page not found.</h2>
           <p>Go back to the documentation index: $link.</p>
        ";
  }

  public static function getFromProfile($profile){
    if (!empty($profile)) {
      if (!empty($profile['args'])) {
        $options = $profile['args'];
      }
      $handler = XtcLoaderHandler::get($profile['type'] . '_' . $profile['verb']);
      $handler->setProfile($profile)
        ->setOptions($options);
      if(!empty($handler)){
        return $handler->processContent();
      }
    }
    return NULL;

  }


}
