<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 20/08/2018
 * Time: 16:37
 */

namespace Drupal\xtcfile\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcfile\XtendedContent\API\WriterFile;

class XtcFileController extends ControllerBase {

  /**
   * @param $alias
   *
   * @return array
   */
  public function file($alias) {
    $values['body'] = '';
    $handler = XtcLoaderHandler::getHandlerFromProfile($alias);
    if (!empty($handler)) {
      $values['body'] = $handler->processContent();
    }
    return [
      '#theme' => 'xtc_file',
      '#response' => $values,
    ];
  }

  public function getTitle() {
    return 'File';
  }

  public function set($alias, $filename = '') {
    $content = XtcLoaderProfile::content('algolia', ['qs' => 'prefix=pari']);
    return WriterFile::write($content, $alias, ['filename' => $filename]);
  }

  public function delete($alias, $filename = '') {
    return XtcLoaderProfile::delete($alias, ['filename' => $filename]);
  }

  protected function getType() {
    return 'file';
  }

}
